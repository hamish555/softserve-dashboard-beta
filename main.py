"""This program is designed for the Python Softserve library.

It will connect to the Jackbords using Softserve in a friendly system.
The interface was designed and tested by Hamish Lester at the Paraparaumu
College robotics club in 2021/2022, Using Lachlan Paulsen's Softserve library
designed for communicating with the Jackbords using python.
Softserve download: https://github.com/NoNameWouldSuffice/new-softserve
My GitLab: https://gitlab.com/hamish555
"""
import tkinter as tk
import softserve as ss
import os
import re
import time
import subprocess
import sys


def clear():
    """Do this to clear the screen."""
    if os.name == 'posix':
        os.system('clear')
    else:
        os.system('cls')


def connect():
    """Do this to connect to the Jackbords."""
    global user
    global users
    users = []
    with open("id.txt", "r") as f:
        lines = f.readlines()
    lines = filter(lambda x: not x.isspace(), lines)
    with open("id.txt", "w") as f:
        f.write("".join(lines))
    f = open('id.txt', 'r')
    for line in f:
        users.append(line.strip())
    print('Connectig With Users:', users)
    f.close()
    try:
        try:
            ss.Jackbord(users[0])
        except IndexError:
            pass
        print('Connect Succeesful, Launching Interface')
    except:
        print('Failed To Read MqttCreds, Please Enter Credentials')
        username = input('Username: ')
        password = input('Password: ')
        f = open('mqttCreds.json', 'w')
        f.write('{"username": "' + username + '", "password": "' +
                password + '"}')
        f.close()
        ss.importCreds()
        clear()


def s_commands(x, *args):
    """Do this to send a single command to the selected Jackbord(s)."""
    if current_id == 'Use All':
        for i in range(len(multi_users)):
            multi_users[i].cmd(x)
            print('Command Sent:', x)
    else:
        print('Command Sent:', x)
        current_user.cmd(x)


def d_commands(x, z, *args):
    """Do this to send two commands to the selected Jackbord(s)."""
    if current_id == 'Use All':
        for i in range(len(multi_users)):
            multi_users[i].cmd(x)
            multi_users[i].cmd(z)
            print('Commands Sent: 1.', x, '2.', z)
    else:
        print('Commands Sent: 1.', x, '2.', z)
        current_user.cmd(x)
        current_user.cmd(z)


def drive():
    """Do this to access the drive page for the Jackbord(s) selected."""
    def drive_to_home():
        drive_page.destroy()
        home()

    def forward(*args):
        global speed, c_forward, c_back, c_left, c_right
        c_forward, c_back, c_left, c_right = True, False, False, False
        command = 'sbms ' + speed + ' ' + speed
        s_commands(command)

    def back(*args):
        global speed, c_forward, c_back, c_left, c_right
        c_forward, c_back, c_left, c_right = False, True, False, False
        command = 'sbms -' + speed + ' -' + speed
        s_commands(command)

    def stop(*args):
        global speed, c_forward, c_back, c_left, c_right
        c_forward, c_back, c_left, c_right = False, False, False, False
        s_commands('stop')

    def left(*args):
        global speed, c_forward, c_back, c_left, c_right
        c_forward, c_back, c_left, c_right = False, False, True, False
        command = 'sbms ' + speed + ' -' + speed
        s_commands(command)

    def right(*args):
        global speed, c_forward, c_back, c_left, c_right
        c_forward, c_back, c_left, c_right = False, False, False, True
        command = 'sbms -' + speed + ' ' + speed
        s_commands(command)

    def throttle(v):
        global speed, c_forward, c_back, c_left, c_right
        speed = v
        if c_forward:
            forward()
        elif c_back:
            back()
        elif c_left:
            left()
        elif c_right:
            right()
        else:
            pass
    home_page.destroy()
    global drive_page, c_forward, c_back, c_left, c_right, speed
    c_forward, c_back, c_left, c_right, speed = \
        False, False, False, False, '25'
    drive_page = tk.Tk()

    drive_page.geometry('400x400')
    drive_page.bind('<Up>', forward)
    drive_page.bind('<Down>', back)
    drive_page.bind('<Left>', left)
    drive_page.bind('<Right>', right)
    drive_page.bind('<space>', stop)
    tk.Scrollbar(drive_page).pack(side=tk.RIGHT, fill=tk.Y)
    tk.Button(drive_page, text='Forward',
              command=forward, width=5, height=3).pack()
    frame = tk.Frame(drive_page)
    frame.pack()
    tk.Button(frame, text='Left', command=left, width=5,
              height=3).pack(side=tk.LEFT)
    tk.Button(frame, text='Right', command=right, width=5,
              height=3).pack(side=tk.RIGHT)
    tk.Button(frame, text='Stop', command=stop, width=5, height=3).pack()
    tk.Button(drive_page, text='Back', command=back, width=5, height=3).pack()
    tk.Scale(drive_page, from_=25, to=100, command=throttle,
             orient=tk.HORIZONTAL, length=200).pack()
    tk.Button(drive_page, text='Home',
              command=drive_to_home).pack(side=tk.BOTTOM, fill=tk.X)

    drive_page.mainloop()


def terminal():
    """Do this to access the terminal for the selected Jackbord(s)."""
    def terminal_to_home():
        terminal_page.destroy()
        home()

    def send(v):
        s_commands(command_input.get())
        command_input.delete(0, 'end')
    home_page.destroy()
    global terminal_page
    terminal_page = tk.Tk()

    terminal_page.geometry('400x400')

    tk.Scrollbar(terminal_page).pack(side=tk.RIGHT, fill=tk.Y)
    command_input = tk.Entry(terminal_page)
    command_input.bind('<Return>', send)
    command_input.pack()
    tk.Button(terminal_page, text='Home', command=terminal_to_home).pack()
    tk.Button(terminal_page, text='Send Command', command=lambda s=None:
              send(s)).pack()

    terminal_page.mainloop()


def writer():
    """Do this to access the writer page to use the custom code."""
    def writer_to_sel_file():
        writer_page.destroy()
        sel_file()

    def save():
        with open(current_file, 'w') as f:
            f.write(code.get('1.0', 'end'))

    def run():
        line_num = 0
        clear()
        writer_page.wm_state('iconic')
        variables = {}
        write_py = False
        with open(current_file, 'r') as f:
            for file_line in f:
                line_num += 1
                line = file_line
                if write_py is False:
                    for i in range(len(line)):
                        if line[i].isspace():
                            line = line.strip()
                        else:
                            break
                line = line.rstrip('\n')
                if not line.strip():
                    pass
                elif line.startswith('>>>'):
                    pass
                elif line.startswith('```{'):
                    write_py = True
                    python = open('DO_NOT_TOUCH.py', 'w')
                elif line.startswith('}```'):
                    write_py = False
                    python.close()
                    subprocess.check_call(['python', 'DO_NOT_TOUCH.py'],
                                          stdout=sys.stdout,
                                          stderr=subprocess.STDOUT)
                elif write_py is True:
                    line = line + '\n'
                    python.write(line)
                elif line.__contains__('=') and line[0] != '~':
                    if line.__contains__('#'):
                        data = line.rstrip('\n').split().lstrip('"').rstrip('"').lstrip("'").rstrip("'")
                        data.remove('=')
                        data.remove('#')
                        try:
                            data_name = data[0]
                            data_list = ''
                            for i in range(len(data) - 1):
                                i += 1
                                data_list = data_list + ' ' + data[i]
                            data_list = input(data_list.lstrip())
                            variables[data_name] = data_list
                        except IndexError:
                            data_list = ''
                            for i in range(len(data) - 1):
                                i += 1
                                data_list = data_list + ' ' + data[i]
                            input(data_list.lstrip())
                    else:
                        data = line.rstrip('\n').split()
                        data.remove('=')
                        data_name = data[0]
                        data_list = ''
                        for i in range(len(data) - 1):
                            i += 1
                            data_list = data_list + ' ' + data[i]
                        variables[data_name] = data_list
                elif line.startswith('@'):
                    pass
                elif line.startswith('!'):
                    pass
                elif line.startswith('%'):
                    time.sleep(float(line.lstrip('% ')))
                elif line.startswith("'") or line.startswith('"'):
                    line = line.lstrip('"').rstrip('"')
                    line = line.lstrip("'").rstrip("'")
                    print(line)
                elif line.startswith('$'):
                    command = line.lstrip('$ ')
                    s_commands(command)
                elif line == 'clear':
                    os.system('clear')
                else:
                    try:
                        line = variables[line.strip('\n')]
                        line = line.lstrip()
                        line = line.lstrip("'").rstrip("'")
                        line = line.lstrip('"').rstrip('"')
                        print(line)
                    except KeyError:
                        print(f'Invalid Syntax on Line {line_num}:', line)
        print('Script Finished, Reopen Interface')
    global writer_page
    writer_page = tk.Tk()

    writer_page.geometry('2130x2130')

    code = tk.Text(writer_page)
    with open(current_file, 'r') as f:
        for line in f:
            code.insert(tk.INSERT, line)
    code.pack(fill=tk.BOTH)
    tk.Button(writer_page, text='Save', command=save).pack()
    tk.Button(writer_page, text='Run', command=run).pack()
    tk.Button(writer_page, text='File Selection',
              command=writer_to_sel_file).pack()

    writer_page.mainloop()


def sel_file():
    """Do this to select a file to use in the writer page."""
    global files

    def sel_file_to_home():
        sel_file_page.destroy()
        home()

    def add_file():
        global files
        sel_file_page.destroy()
        clear()
        if current_id == 'Use All':
            print('Selected Users: All')
        else:
            print('Selected User:', current_id)
        file_name = input('File Name: ')
        file_name = file_name + '.txt'
        if files.__contains__(file_name):
            print('File Already Exists. Do you want to overwrite file?\
                  \n\'Y\' or \'N\'')
            a = input()
            a = a.lower()
            if a[0] == 'y':
                file_name = 'Custom_Code_Files/' + file_name
                with open(file_name, 'w') as f:
                    f.write('\'Hello World\'')
                sel_file()
            elif a[0] == 'n':
                sel_file()
        else:
            file_name = 'Custom_Code_Files/' + file_name
            with open(file_name, 'w') as f:
                f.write('\'Hello World\'')
            sel_file()

    def file_sel(x):
        global current_file
        try:
            current_file = file_list.get(file_list.curselection())
            clear()
            if current_id == 'Use All':
                print('Selected Users: All')
            else:
                print('Selected User:', current_id)
            print('Selected File:', current_file)
            current_file = 'Custom_Code_Files/' + current_file
            sel_file_page.destroy()
            writer()
        except tk.TclError:
            pass
    clear()
    if current_id == 'Use All':
        print('Selected Users: All')
    else:
        print('Selected User:', current_id)
    files = os.listdir('Custom_Code_Files/')
    try:
        home_page.destroy()
    except tk.TclError:
        pass
    global sel_file_page
    sel_file_page = tk.Tk()

    sel_file_page.geometry('400x400')

    tk.Button(sel_file_page, text='Add File',
              command=add_file).pack(side=tk.BOTTOM, fill=tk.X)
    tk.Button(sel_file_page, text='Select File', command=lambda s=None:
              file_sel(s)).pack(side=tk.BOTTOM, fill=tk.X)
    tk.Button(sel_file_page, text='Home',
              command=sel_file_to_home).pack(side=tk.BOTTOM, fill=tk.X)
    scroll_bar = tk.Scrollbar(sel_file_page, width=15)
    file_list = tk.Listbox(sel_file_page, yscrollcommand=scroll_bar.set,
                           width=20)
    file_list.bind('<Double-Button>', file_sel)
    file_list.bind('<Return>', file_sel)
    for i in range(len(files)):
        i -= 1
        file_list.insert(tk.END, files[i])
    scroll_bar.pack(side=tk.RIGHT, fill=tk.Y)
    file_list.pack(side=tk.RIGHT, fill=tk.BOTH, ipadx=999)
    scroll_bar.config(command=file_list.yview)

    sel_file_page.mainloop()


def home():
    """Do this to access the main home page."""
    global home_page
    clear()
    if current_id == 'Use All':
        print('Selected Users: All')
    else:
        print('Selected User:', current_id)
    home_page = tk.Tk()

    home_page.geometry('400x400')

    tk.Scrollbar(home_page).pack(side=tk.RIGHT, fill=tk.Y)
    tk.Button(home_page, text='Change User', command=set_pro).pack()
    tk.Button(home_page, text='Drive', command=drive).pack()
    tk.Button(home_page, text='Terminal', command=terminal).pack()
    tk.Button(home_page, text='Code Writer', command=sel_file).pack()

    home_page.mainloop()


def set_pro():
    """Do this to select a profile(s) to use with the Jackbord(s)."""
    def profile_sel(x):
        global current_id
        global current_user
        global multi_users
        try:
            current_id = profile_list.get(profile_list.curselection())
            if current_id == 'Use All':
                multi_users = {}
                for i in range(len(users)):
                    current_user = ss.Jackbord(users[i])
                    multi_users[i] = current_user
                print('Selected Users: All')
            else:
                current_user = ss.Jackbord(current_id)
                clear()
                print('Selected User:', current_id)
            profile_page.destroy()
            home()
        except tk.TclError:
            pass

    def add_profile():
        profile_page.destroy()
        clear()
        new_id = input('ID: ')
        pattern = re.compile("^[a-zA-Z0-9]{4,6}$")
        if pattern.search(new_id) is None:
            print('Invalid Profile')
        else:
            with open('id.txt', 'a') as f:
                f.write('\n')
                f.write(new_id)
            print('Profile Added')
        connect()
        set_pro()
    try:
        home_page.destroy()
    except:
        pass
    global profile_page
    profile_page = tk.Tk()

    profile_page.geometry('400x400')
    profile_page.config(bg='lightBlue')

    tk.Button(profile_page, text='Add Profile',
              command=add_profile).pack(side=tk.BOTTOM, fill=tk.X)
    tk.Button(profile_page, text='Select Profile', command=lambda s=None:
              profile_sel(s)).pack(side=tk.BOTTOM, fill=tk.X)
    scroll_bar = tk.Scrollbar(profile_page, width=15)
    profile_list = tk.Listbox(profile_page, yscrollcommand=scroll_bar.set,
                              width=20)
    profile_list.bind('<Double-Button>', profile_sel)
    profile_list.bind('<Return>', profile_sel)
    for i in range(len(users)):
        i -= 1
        profile_list.insert(tk.END, users[i])
    profile_list.insert(tk.END, 'Use All')
    image = tk.PhotoImage(file='Media/jackbord.png')
    try:
        tk.Label(profile_page, image=image, bg='lightBlue').pack(side=tk.LEFT,
                                                                 padx=50)
    except tk.TclError:
        clear()
        print('Restart to load mqttCreds')
        quit()
    scroll_bar.pack(side=tk.RIGHT, fill=tk.Y)
    profile_list.pack(side=tk.RIGHT, fill=tk.BOTH, ipadx=999)
    scroll_bar.config(command=profile_list.yview)

    profile_page.mainloop()


clear()
connect()
set_pro()
